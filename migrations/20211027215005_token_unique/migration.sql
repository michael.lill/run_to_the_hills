/*
  Warnings:

  - A unique constraint covering the columns `[confirmationToken]` on the table `Run` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[confirmationToken]` on the table `Subscription` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Run_confirmationToken_key" ON "Run"("confirmationToken");

-- CreateIndex
CREATE UNIQUE INDEX "Subscription_confirmationToken_key" ON "Subscription"("confirmationToken");
