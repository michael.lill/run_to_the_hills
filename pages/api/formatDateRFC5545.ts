import { format } from "date-fns";
import { de } from "date-fns/locale";

export function formatDateRFC5545(date: Date) {
  return format(date, "yyyyMMdd'T'HHmmss'Z'", {
    locale: de
  });
}
