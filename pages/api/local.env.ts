export default {
  MAIL_HOST: process.env.MAIL_HOST || 'mailcatcher',
  MAIL_PORT: parseInt(process.env.MAIL_PORT || '') || 1025,
  MAIL_USER: process.env.MAIL_USER || '',
  MAIL_PASSWORD: process.env.MAIL_PASSWORD || '',
  HOST: process.env.HOST || 'localhost:3000'
}