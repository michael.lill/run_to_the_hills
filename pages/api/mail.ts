
import settings from './local.env';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  host: settings.MAIL_HOST,
  port: settings.MAIL_PORT,
  secure: true,
  auth: {
    user: settings.MAIL_USER,
    pass: settings.MAIL_PASSWORD
  }
});

export async function sendMail(to: any, subject: any, html: any, attachment: any) {
  const mailOptions: any = {
    from: settings.MAIL_USER, // sender address
    bcc: to, // list of receivers
    subject, // Subject line
    html,
  };

  if (attachment) {
    mailOptions.attachments = [attachment];
  }

  try{
    return await transporter.sendMail(mailOptions);
  }catch(e){
    console.log(e);
    return false;
  }
};


