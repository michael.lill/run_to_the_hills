import type { NextApiRequest, NextApiResponse } from 'next';
import { runs } from "./runs";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {

  const allRuns = await runs();

  res.status(200).json(allRuns)
}
