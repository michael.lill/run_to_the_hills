import { Run } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { prismaClient } from "../prisma";
import settings from '../local.env';

export default async function handle(req: NextApiRequest,
  res: NextApiResponse<Run>) {
  const confirmationToken = req.query.confirmation_token as string;
  await prismaClient().run.update({
    where: { confirmationToken: confirmationToken } as any,
    data: {
      confirmationToken:null
    }
  })

  res.statusCode = 302;
  res.setHeader('Location',`https://${settings.HOST}`)
  res.end()
}