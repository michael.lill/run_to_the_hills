import { Run } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { prismaClient } from "../prisma";

// NYI require deletionId?
export default async function handle(req: NextApiRequest,
  res: NextApiResponse<Run>) {
  const runId = req.query.id || req.body.id;
  const result = await prismaClient().run.delete({
    where: { id: runId },
  })
  res.json(result)
}