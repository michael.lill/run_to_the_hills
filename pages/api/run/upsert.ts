import { Run } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { v4 } from 'uuid';
import { formatDateDe } from '../formatDateDe';
import { sendMail } from '../mail';
import { prismaClient } from "../prisma";
import { validateEmail } from '../validateEmail';

import settings from '../local.env';

// NYI validation
export default async function handle(req: NextApiRequest,
  res: NextApiResponse<any>) {
  const run = req.body.run as Run;
  delete (run as any).participants;
  const runId = run.id;
  const email = req.body.user?.email;
  // update
  if (runId !== -1) {
    await prismaClient().run.update({
      where: { id: runId },
      data: {
        ...run,
        id: undefined,
        participants: email !== '' ? {
          connectOrCreate: {
            create: {
              email: email
            },
            where: {
              email: email
            }
          }
        } : undefined
      } as any,
      include: {
        participants: email !== '',
      }
    })
    res.json(undefined)
  }
  // insert
  else {
    if (!validateEmail(email)) {
      res.statusCode = 500;
      res.json({ error: 'email failed validation' });
      return;
    }
    const insertedRun = await prismaClient().run.create({
      data: {
        ...run,
        id: undefined,
        confirmationToken: v4(),
        creator: {
          connectOrCreate: {
            create: {
              email: email
            },
            where: {
              email: email,
            },
          }
        }
      } as any,
      include: {
        creator: true,
      }
    });
    await sendMail(email, 'Bestätige das Anlegen des Trainings',
      `${formatDateDe(insertedRun.startDate)} - ${insertedRun.title} - ${insertedRun.description}<br /><a href="https://${settings.HOST}/api/run/confirm?confirmation_token=${insertedRun.confirmationToken}">Veröffentlichen</a>`
      , null);
    res.json(undefined)
  }


}