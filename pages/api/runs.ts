import { addDays, subMonths } from "date-fns";
import { prismaClient } from "./prisma";


export async function runs() {
  return await prismaClient().run.findMany({
    where: {
      startDate: {
        gte: subMonths(new Date(), 1),
      },
      confirmationToken: {
        equals: null
      }
    },
    select: {
      id: true,
      title: true,
      startDate: true,
      description: true,
      participants: {
        select: {
          id: true
        }
      }
    }
  });
}
