import { NextPageContext } from 'next';
import React from 'react';
import { runs } from "./api/runs";
import { formatDateDe } from "./api/formatDateDe";
import settings from './api/local.env';

export default class Rss extends React.Component {
  static async getInitialProps({ res }: NextPageContext) {
    if (!res) {
      return;
    }
    res.setHeader("Content-Type", "text/xml");
    res.write(getRssXml(await runs()));
    res.end();
  }
}

// NYI
function getRssXml(runs: any[]) {
  return `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>urn:uuid:47a198d8-3bd2-49ac-9c90-57b776e0fdae</id>
    <title type="text">Trainingskalender</title>
    <subtitle type="html">Der Trainingskalender für Vereine</subtitle>
    <updated>${new Date().toISOString()}</updated>
    ${runs.map(r => run(r)).join('\n')}
</feed>`
}

// NYI
function run(run: any) {
  return `<entry>
  <title type="html"><![CDATA[${formatDateDe(run.startDate)} ${htmlEncode(run.title)}]]></title>
  <author><name>none</name></author>
  <id>http://${settings.HOST}/?runid=${run.id}/</id>
  <updated>${new Date().toISOString()}</updated>
  <published>${new Date().toISOString()}</published>
  <summary type="html"><![CDATA[${formatDateDe(run.startDate)} ${htmlEncode(run.description)}]]></summary>
</entry>`;
}

function htmlEncode(str: string){
  return str
  .replace(/[\u00A0-\u9999<>\&]/gim, (i) => '&#' + i.charCodeAt(0) + ';')
  .replace(new RegExp('\n', 'g'), () => '<br />');
}