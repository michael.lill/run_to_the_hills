import { add } from 'date-fns';
import { NextPageContext } from 'next';
import React from 'react';
import settings from './api/local.env';
import { runs } from "./api/runs";
import { formatDateRFC5545 } from './api/formatDateRFC5545';

export default class Ics extends React.Component {
  static async getInitialProps({ res }: NextPageContext) {
    if (!res) {
      return;
    }
    res.setHeader("Content-Type", "text/calendar; charset=utf-8");
    res.write(getIcs(await runs()));
    res.end();
  }
}

function getIcs(runs: { id: number; title: string; startDate: Date; description: string | null; participants: { id: number; }[]; }[]): any {
  return `BEGIN:VCALENDAR
PRODID:-//No Inc//run to the hills//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-TIMEZONE:Europe/Berlin
BEGIN:VTIMEZONE
TZID:Europe/Berlin
X-LIC-LOCATION:Europe/Berlin
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
${runs.map(run => vevent(run)).join('\n')}
END:VCALENDAR
`.replace(new RegExp('\n', 'g'), () => '\r\n');
}

function encode(str: string | null) {
  if (!str) {
    return "";
  }
  return str
    .replace(new RegExp('\\\\', 'g'), () => '\\\\')
    .replace(new RegExp(';', 'g'), () => '\\;')
    .replace(new RegExp(',', 'g'), () => '\\,')
    .replace(new RegExp('\n', 'g'), () => '\\n');
}

function vevent(run: { id: number; title: string; startDate: Date; description: string | null; participants: { id: number; }[]; }) {
  return `BEGIN:VEVENT
UID:${run.id}@${settings.HOST}
DTSTART:${formatDateRFC5545(run.startDate)}
DTEND:${formatDateRFC5545(add(run.startDate, { hours: 2 }))}
DTSTAMP:${formatDateRFC5545(new Date())}
DESCRIPTION:${encode(run.description)}
SUMMARY:${encode(run.title)}
ORGANIZER;CN=NONE:MAILTO:noreply@${settings.HOST}
END:VEVENT`;
}
