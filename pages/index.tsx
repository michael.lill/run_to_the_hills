import FullCalendar, { EventClickArg, EventInput, EventSourceInput } from '@fullcalendar/react';
import deFC from '@fullcalendar/core/locales/de';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, { DateClickArg } from '@fullcalendar/interaction';
import Close from '@mui/icons-material/Close';
import Edit from '@mui/icons-material/Edit';
import Help from '@mui/icons-material/Help';
import RssFeed from '@mui/icons-material/RssFeed';
import Event from '@mui/icons-material/Event';
import Save from '@mui/icons-material/Save';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import DateTimePicker from '@mui/lab/DateTimePicker';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { Alert, Button, Snackbar, Stack, TextField } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import { Run, User } from '@prisma/client';
import { addDays, addHours, compareAsc, isBefore, parseISO } from 'date-fns';
import { de } from 'date-fns/locale';
import type { NextPage } from 'next';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import styles from '../styles/Home.module.css';
import { formatDateDe } from "./api/formatDateDe";
import { validateEmail } from "./api/validateEmail";

enum ViewType {
  List, Calendar
}

const Api = {

  Queries: {
    fetchRuns: async () => {
      return await fetch(`/api/run`)
        .then((resp) => resp.json())
        .then(data => {
          data.map((run: Run) => {
            run.startDate = parseISO(run.startDate as any)
            return run;
          })
          return data;
        });
    }
  },

  Commands: {
    upsertRun: async (run: Run, user?: User) => {
      await fetch(`/api/run/upsert`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          run,
          user,
        }),
      });
    },

    deleteRun: async (run: Run) => {
      return await fetch(`/api/run/delete`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(run),
      });
    }
  }

}

const Home: NextPage = () => {
  const [runs, setRuns] = useState([] as Run[]);
  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");

  useEffect(() => {
    Api.Queries.fetchRuns().then(data => setRuns(data));
  }, []);

  const [selectedRun, setSelectedRun] = useState(undefined as Run | undefined);
  const [view, setView] = useState(ViewType.Calendar)

  const onDateClick = (args: DateClickArg) => {
    if (isBefore(args.date, addDays(new Date(), -1))) {
      showSnack("Datum liegt in der Vergangenheit");
      return;
    }
    setSelectedRun({
      id: -1,
      description: '',
      startDate: addHours(args.date, 18),
    } as Run);
  }

  const onRunClick = (args: EventClickArg) => {
    setSelectedRun(args.event.extendedProps as Run);
  }

  const showSnack = (message: string) => {
    setSnackbarMessage(message);
    setSnackBarOpen(true);
  }

  function tranform(runs: Run[]): EventSourceInput {
    return runs.map(run => {
      return {
        allDay: false,
        start: run.startDate.toISOString(),
        backgroundColor: runColor(run),
        title: run.title ?? '',
        extendedProps: run,
      } as EventInput;
    });
  }

  return (
    <div className={styles.container}>
      <Snackbar
        open={snackBarOpen}
        autoHideDuration={5000}
        onClose={() => setSnackBarOpen(false)}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert severity="info">
          {snackbarMessage}
        </Alert>
      </Snackbar>
      <Head>
        <title>Trainingsplaner</title>
        <meta name="description" content="Der Trainingsplaner für Vereine" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="/feed" />
      </Head>
      <header style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', gap: '1em' }} className="mb-5">
        <div>Zeitzone: {Intl.DateTimeFormat().resolvedOptions().timeZone}</div>

        <div>
          {view === ViewType.Calendar &&
            <Button variant="contained" onClick={() => setView(ViewType.List)}>
              Listenansicht
            </Button>
          }
          {view === ViewType.List &&
            <Button variant="contained" onClick={() => setView(ViewType.Calendar)}>
              Kalenderansicht
            </Button>
          }
          <Button variant="contained" style={{ marginLeft: '5px' }} onClick={() => {
            location.href = '/feed';
          }} color="success" aria-label="close">
            <RssFeed />
          </Button>
          <Button variant="contained" style={{ marginLeft: '5px' }} onClick={() => {
            location.href = '/ics';
          }} color="success" aria-label="close">
            <Event />
          </Button>
          <Button variant="contained" style={{ marginLeft: '5px' }} onClick={() => {
            window.alert('NYI');
          }} color="success" aria-label="close">
            <Help />
          </Button>
        </div>
      </header>
      <main className={styles.main}>

        {view === ViewType.Calendar &&
          <FullCalendar
            plugins={[dayGridPlugin, interactionPlugin]}
            initialView={"dayGridMonth"}
            height="100%"
            weekNumbers
            firstDay={1}
            locale={deFC}
            fixedWeekCount={false}
            selectable
            eventTimeFormat={{
              hour: '2-digit',
              minute: '2-digit',
              hour12: false
            }}
            eventClick={onRunClick}
            dateClick={onDateClick}
            events={tranform(runs)}
          />
        }
        {view === ViewType.List &&
          <Stack spacing={2}>
            {runs.sort((a, b) => {
              return compareAsc(a.startDate, b.startDate)
            }).map((run, idx) => <div key={idx} onClick={() => setSelectedRun(run)} style={{ cursor: 'pointer', display: 'grid', gridGap: '0.5em', gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))', border: '2px outset rgba(0,0,0,0.3)', padding: '0.25em' }}>
              <RunSummary run={run} />
            </div>)}
          </Stack>
        }

        {selectedRun &&
          <RunDialog setSelectedRun={setSelectedRun} selectedRun={selectedRun} fetchRuns={async () => {
            setRuns(await Api.Queries.fetchRuns());
          }} showSnack={showSnack} />
        }

      </main>

      <footer className={styles.footer}>
        <ul>
          <li style={{ color: 'red' }}>0-1 rot</li>
          <li style={{ color: 'orange' }}>2-3 orange</li>
          <li style={{ color: 'green' }}>&gt;3 grün</li>
        </ul>
      </footer>
    </div>
  )
}

function RunSummary({ run }: { run: any }) {
  return <Stack spacing={2}>
    <div>{formatDateDe(run.startDate)}</div>
    <div>{run.title}</div>
    <div>{run.description}</div>
    <div>Teilnehmende: {run.participants.length}</div>
  </Stack>
}

function RunView({ selectedRun, setSelectedRun, fetchRuns, showSnack }: { selectedRun: Run, setSelectedRun: Function, fetchRuns: Function, showSnack: Function }) {

  function participate(selectedRun: Run): void {
    const email = (document.querySelector('#participant-email') as HTMLInputElement)?.value ?? '';
    if (!validateEmail(email)) {
      showSnack('Bitte valide E-Mail-Adresse eingeben.');
      return;
    }
    Api.Commands.upsertRun(
      selectedRun,
      {
        id: -1,
        email: email
      }
    ).then(() => fetchRuns()).then(() => setSelectedRun(undefined))
  }

  return <>
    <div className="mb-5">
      <RunSummary run={selectedRun} />
    </div>
    <div className="card">
      <Stack spacing={2}>
        <div>
          <TextField id="participant-email" required fullWidth defaultValue="" label="E-Mail" variant="standard" />
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button variant="contained" color="success" onClick={() => participate(selectedRun)}>
            Teilnehmen
          </Button>
        </div>
      </Stack>
    </div>
  </>
}

function RunEditor({ run, setRun }: { run: Run, setRun: Function }) {
  return <Stack spacing={2}>
    <div>
      <LocalizationProvider dateAdapter={AdapterDateFns} locale={de}>
        <DateTimePicker disablePast minutesStep={5} label="Wann" value={run.startDate} onChange={(newValue) => {
          if (!newValue) {
            return;
          }
          setRun({
            ...run,
            startDate: newValue
          });
        }} renderInput={(props) => <TextField {...props} />} />
      </LocalizationProvider>
    </div>
    <div>
      <TextField id="run-title" fullWidth required defaultValue={run.title} label="Titel" variant="standard" />
    </div>
    <div>
      <TextField id="run-description" fullWidth required multiline minRows={3} defaultValue={run.description} label="Beschreibung" variant="standard" />
    </div>
  </Stack>;
}

function RunDialog({ selectedRun, setSelectedRun, fetchRuns, showSnack }: { selectedRun: Run, setSelectedRun: Function, fetchRuns: Function, showSnack: Function }) {

  const [editMode, setEditMode] = useState(selectedRun.id === -1);

  return <Dialog open maxWidth="sm" fullWidth>

    <div style={{ padding: '1em' }}>
      {editMode &&
        <h2>Training anlegen/editieren</h2>
      }
      <div className="mb-5" style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div>
          {!editMode &&
            <Button variant="contained" onClick={() => setEditMode(!editMode)} aria-label="close">
              <Edit />
            </Button>
          }
        </div>
        <Button variant="contained" onClick={() => setSelectedRun(undefined)} aria-label="close">
          <Close />
        </Button>
      </div>
      {editMode &&
        <>
          <RunEditor run={selectedRun} setRun={setSelectedRun} />
          {(selectedRun.id === -1) &&
            <div className="mb-5">
              <TextField id="creator-email" required fullWidth defaultValue="" label="E-Mail" variant="standard" />
            </div>
          }
        </>
      }
      {!editMode &&
        <RunView setSelectedRun={setSelectedRun} selectedRun={selectedRun} fetchRuns={fetchRuns} showSnack={showSnack} />
      }
      {editMode &&
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
          </div>
          <Button variant="contained" onClick={() => {
            const title = (document.querySelector('#run-title') as any)?.value ?? '';
            const description = (document.querySelector('#run-description') as any)?.value ?? '';
            const email = (document.querySelector('#creator-email') as HTMLInputElement)?.value ?? '';
            if (title.length < 1) {
              return showSnack("Titel fehlt?")
            } if (description.length < 1) {
              return showSnack("Beschreibung fehlt?")
            } if (selectedRun.id === -1 && email.length < 1) {
              return showSnack("E-Mail fehlt?")
            }
            setSelectedRun(undefined);
            Api.Commands.upsertRun({
              ...selectedRun,
              title,
              description
            }, { id: -1, email: email }).then(() => fetchRuns());
            if (selectedRun.id === -1) {
              showSnack("Du solltest in Kürze eine E-Mail bekommen.")
            }
          }} color="success" aria-label="close">
            <Save />
          </Button>
        </div>
      }
    </div>
  </Dialog >;
}

function runColor(run: any): string | undefined {
  if (run.participants.length < 2) {
    return 'red'
  }
  if (run.participants.length < 4) {
    return 'orange'
  }
  return 'green'
}

export default Home