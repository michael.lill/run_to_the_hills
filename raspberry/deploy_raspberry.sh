#!/bin/bash
set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH/..

RASPBERRY=rtth@raspberrypi
WORK_DIR=/home/rtth/rtth

rsync -avz -e 'ssh' --exclude=dist.sqlite --delete .next public package.json package-lock.json schema.prisma migrations raspberry/Dockerfile raspberry/docker-compose.yaml .env $RASPBERRY:$WORK_DIR

#build application image
ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker build -t rtth:latest -f Dockerfile . --pull'

ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker-compose down || true'
ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker-compose run -d -w /usr/src/app rtth'
